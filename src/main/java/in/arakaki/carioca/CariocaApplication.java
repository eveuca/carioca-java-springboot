package in.arakaki.carioca;

import in.arakaki.carioca.config.PersistenceJPAConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication(scanBasePackages={"in.arakaki.carioca"})
@EntityScan
public class CariocaApplication{
	public static void main(String[] args){
		SpringApplication.run(CariocaApplication.class, args);
	}
}

