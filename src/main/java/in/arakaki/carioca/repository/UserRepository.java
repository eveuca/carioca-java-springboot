package in.arakaki.carioca.repository;

import in.arakaki.carioca.business_rules.enterprise.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("userRepository")
public interface UserRepository extends JpaRepository<User, Integer> {

}
